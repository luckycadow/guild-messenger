import React from 'react';
import { IMessage } from './Messenger';
import styles from './Message.module.css';

const Message: React.FC<IMessageProps> = ({ message, sent }: IMessageProps) => {

    const typeClass = sent ? styles.sent : styles.received;

    return (
    <div className={`${styles.container} ${typeClass}`}>
        <div>
            <small>{sent ? 'you' : message.user} at {new Date(message.timestamp).toLocaleTimeString()}</small>
        </div>
        <pre className={styles.body}>{message.body}</pre>
    </div>
    );
};

export default Message;

export interface IMessageProps {
    message: IMessage;
    sent: boolean;
}