import React from 'react';
import Login from './Login';
import Messenger from './Messenger';

const USER_STORAGE_KEY = 'messenger-user';

export default class App extends React.Component<{}, IAppState> {
    public state = {
        username: localStorage.getItem(USER_STORAGE_KEY)
    };

    public render() {
        const { username } = this.state;

        if (!username) {
            return <Login onSubmit={this.handleLogin} />;
        }

        return <Messenger user={username} />;
    }

    private handleLogin = (username: string) => {
        localStorage.setItem(USER_STORAGE_KEY, username);
        this.setState({ username });
    }
}

interface IAppState {
    username: string | null;
}
