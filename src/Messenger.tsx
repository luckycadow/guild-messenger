import { Button, Form, Input } from 'antd';
import FormItem from 'antd/lib/form/FormItem';
import React from 'react';
import io from 'socket.io-client';
import Message from './Message';
import styles from './Messenger.module.css';

const Messenger: React.FC<IMessengerProps> = ({ user }: IMessengerProps) => {
    const scrollContainer = React.useRef<HTMLDivElement>(null);
    const [socket] = React.useState<SocketIOClient.Socket>(io());
    const [messages, setMessages] = React.useState<IMessage[]>([]);
    const [typers, setTypers] = React.useState<string[]>([]);
    const [draft, setDraft] = React.useState('');

    React.useEffect(() => {
        socket.once('incoming-message', (message: IMessage) => {
            setMessages([...messages, message]);
            if (scrollContainer.current) {
                scrollContainer.current.scrollTo({
                    top: scrollContainer.current.scrollHeight,
                    behavior: 'smooth'
                });
            }
        });
    }, [messages, socket]);

    React.useEffect(() => {
        socket.once('users-typing', setTypers);
    }, [socket, typers]);

    const sendMessage = (e?: React.FormEvent) => {
        if (e) {
            e.preventDefault();
        }

        const body = draft.trim();
        if (body) {
            socket.emit('send-message', { user, body });
        }

        setDraft('');
    };

    const handleInput = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setDraft(e.target.value);
        socket.emit('typing', user);
    };

    const handleEnter = (e: React.KeyboardEvent) => {
        if (!e.shiftKey) {
            e.preventDefault();
            sendMessage();
        }
    };

    const formatTypers = () => {
        const otherTypers = typers.filter((t) => t !== user);
        console.log(JSON.stringify(otherTypers));
        switch (otherTypers.length) {
            case 0:
                return <>&nbsp;</>;
            case 1:
                return `${otherTypers[0]} is typing`;
            case 2:
                return `${otherTypers[0]} and ${otherTypers[1]} are typing`;
            case 3:
            case 4:
                return `several people are typing`;
            default:
                return `too many people are typing`;
        }
    };

    return (
        <div className={styles.container}>
            <h1>Guild Messenger</h1>
            <div className={styles.messages} ref={scrollContainer}>
                {messages.map((m) => (
                    <Message key={m.timestamp} message={m} sent={user === m.user} />
                ))}
            </div>
            <Form onSubmit={sendMessage}>
                <small>{formatTypers()}</small>
                <FormItem className={styles.input}>
                    <Input.TextArea
                        placeholder="Enter a message"
                        value={draft}
                        onChange={handleInput}
                        onPressEnter={handleEnter}
                    />
                </FormItem>
                <Button className={styles.button} type="primary" htmlType="submit" size="large">
                    Send
                </Button>
            </Form>
        </div>
    );
};

export default Messenger;

interface IMessengerProps {
    user: string;
}

export interface IMessage {
    user: string;
    body: string;
    timestamp: number;
}
