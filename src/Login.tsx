import { Button, Form, Input } from 'antd';
import React from 'react';
import styles from './Login.module.css';

const Login: React.FC<ILoginProps> = ({ onSubmit }: ILoginProps) => {
    const [userName, setUserName] = React.useState('');

    return (
        <div className={styles['form-container']}>
            <Form className={styles.form} onSubmit={() => onSubmit(userName)}>
                <Form.Item label="Select a user name to start chatting" colon={false}>
                    <Input
                        minLength={3}
                        value={userName}
                        onChange={(e) => setUserName(e.target.value.replace(/[^a-z0-9]*/gi, ''))}
                    />
                </Form.Item>
                <Button
                    type="primary"
                    disabled={userName.length < 3}
                    htmlType="submit"
                    block={true}
                >
                    Get Started
                </Button>
            </Form>
        </div>
    );
};

export default Login;

export interface ILoginProps {
    onSubmit: (username: string) => any;
}
