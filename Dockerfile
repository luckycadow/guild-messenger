FROM node:11

WORKDIR /app

COPY . /app

RUN yarn build

EXPOSE 3000

CMD ["yarn", "start"]