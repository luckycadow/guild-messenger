const express = require('express');

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static('build'));

// Track who is typing
const typingUsers = new Map();

io.on('connection', socket => {
    socket.on('typing', setTypingUser);

    socket.on('send-message', ({ body, recipient, user }) => {
        const message = {
            body,
            recipient,
            user,
            timestamp: Date.now()
        };

        clearTypingUser(user);

        io.emit('incoming-message', message);
    });
});

const port = process.env.PORT ? parseInt(process.env.PORT) : 3000;

http.listen(port, () => {
    console.log(`listening on *:${port}`);
});

function clearTypingUser(user, suppresUpdate = false) {
    if (typingUsers.has(user)) {
        clearTimeout(typingUsers.get(user));
        typingUsers.delete(user);
    }
    if (!suppresUpdate) {
        io.emit('users-typing', [...typingUsers.keys()]);
    }
}

function setTypingUser(user) {
    clearTypingUser(user, true);
    const timer = setTimeout(() => clearTypingUser(user), 1500);
    typingUsers.set(user, timer);
    io.emit('users-typing', [...typingUsers.keys()]);
}
