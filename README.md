This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It uses a small Express & Socket.io server for messaging and React with TypeScript on the front end.

## Available Scripts

In the project directory, you can run:

### `npm start`

Starts the node server used for websocket communcation.<br>
This does not build the app first, to run the app locally see `npm run local`.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run local`

Builds the react app and then runs the node server.<br>
If you want to run the app locally, this is what you're looking for.

## Docker

If docker is more your speed you can run:
`docker build . -t guild-messenger && docker run -p 3000:3000 guild-messenger`
